﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sliding_Door_Script : MonoBehaviour
{
    public GameObject trigger;
    public GameObject leftDoor;
    public GameObject rightDoor;
    public AudioClip triggerSound;
    AudioSource audioSource;

    Animator leftAnim;
    Animator rightAnim;

    public bool isLocked;
    bool isOpen = false;
    void Start()
    {
        leftAnim = leftDoor.GetComponent<Animator>();
        rightAnim = rightDoor.GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider coll)
    {
        if (!isLocked && !isOpen && coll.gameObject.tag == "Player")
        {
            SlideDoors(true);
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (isOpen && coll.gameObject.tag == "Player")
        {
            SlideDoors(false);
        }
    }

    public void SlideDoors(bool open)
    {
        isOpen = open;
        leftAnim.SetBool("slide", open);
        rightAnim.SetBool("slide", open);
        audioSource.PlayOneShot(triggerSound, 0.7f);
    }
}
