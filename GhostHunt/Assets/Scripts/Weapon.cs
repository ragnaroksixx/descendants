﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    public WeaponData data;
    int currentAmmo;
    float lastTimeShot;
    public Transform weaponMuzzle;
    public Vector3 muzzleWorldVelocity { get; private set; }
    public WeaponHandler Owner { get => owner; }
    public int CurrentAmmo { get => currentAmmo; set => currentAmmo = value; }
    public Rigidbody RBody { get => rBody; }

    public WeaponPickupVolume wpv;
    Vector3 m_LastMuzzlePosition;
    WeaponHandler owner;
    CoroutineHandler pickUpHandler;

    Rigidbody rBody;
    public Collider physCol;
    public Collider grappleCol;
    public HitBox throwHitBox;
    public Material greyScaleMat;
    Animator anim;
    public CustomAudioClip equipSFX;
    void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        m_LastMuzzlePosition = weaponMuzzle.position;
        pickUpHandler = new CoroutineHandler(this);
        SetPhysics(true);
        rBody.collisionDetectionMode = CollisionDetectionMode.Discrete;
        //RBody.isKinematic = true;
    }
    public void Ammo(int i)
    {
        currentAmmo = Mathf.Min(data.maxAmmo, currentAmmo + i);
        owner.hud.UpdateAmmo(currentAmmo);
    }
    public void SetWeaponData(WeaponData wd)
    {
        data = wd;
        currentAmmo = data.maxAmmo;
        if (wd.overrideColor)
        {
            GetComponentInChildren<MeshRenderer>().material = Instantiate(greyScaleMat);
            GetComponentInChildren<MeshRenderer>().material.color = wd.color;
            GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", wd.color / 10);
        }
    }
    private void Update()
    {
        if (Time.deltaTime > 0)
        {
            muzzleWorldVelocity = (weaponMuzzle.position - m_LastMuzzlePosition) / Time.deltaTime;
            m_LastMuzzlePosition = weaponMuzzle.position;
        }
    }
    public CustomAudioClip emptySFX;
    public void Shoot()
    {
        if (TryShoot())
        {
            lastTimeShot = Time.time;
        }
    }
    public void SetPhysics(bool val)
    {
        physCol.enabled = val;
        if (val)
        {
            if (!rBody)
                rBody = gameObject.AddComponent<Rigidbody>();
            rBody.interpolation = RigidbodyInterpolation.Extrapolate;
            rBody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            pickUpHandler.StartCoroutine(EnablePickup());
        }
        else
        {
            Destroy(rBody);
            rBody = null;
            wpv.gameObject.SetActive(false);
        }
    }
    public void OnThrow(Vector3 dir)
    {
        Vector3 throwDir = dir.normalized;
        owner = null;
        transform.parent = null;
        SetPhysics(true);
        rBody.AddForce(throwDir * 1750);
        rBody.AddTorque(Random.onUnitSphere * 50);
        grappleCol.enabled = true;
        throwHitBox.enabled = true;
    }
    public void OnDrop()
    {
        Vector3 throwDir = owner.weaponCamera.transform.forward;
        owner = null;
        transform.parent = null;
        SetPhysics(true);
        grappleCol.enabled = true;
        rBody.AddForce(throwDir * 250);
    }
    public void OnEquip(WeaponHandler o)
    {
        owner = o;
        pickUpHandler.StopCoroutine();
        SetPhysics(false);
        owner.hud.SetGun(this);
        grappleCol.enabled = false;
        throwHitBox.enabled = false;
        StartCoroutine(resetNextFrame());
    }
    IEnumerator resetNextFrame()
    {
        yield return new WaitForEndOfFrame();
        if (owner)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
    IEnumerator EnablePickup()
    {
        yield return new WaitForSeconds(2);
        wpv.gameObject.SetActive(true);
        throwHitBox.enabled = false;
    }
    bool TryShoot()
    {
        if (lastTimeShot + data.autofireRate < Time.time)
        {
            if (currentAmmo > 0)
            {
                anim?.SetTrigger("shoot");
                ShootProjectile();
                LevelManager.Instance.stats.ammo += data.ammoPerFire;
                if (!ModifierHandler.Instance.infiniteAmmo)
                    currentAmmo -= data.ammoPerFire;
                if (currentAmmo < 1)
                    emptySFX.Play(weaponMuzzle.transform.position);
                owner.hud.UpdateAmmo(currentAmmo);
                data.fireSFX.Play(weaponMuzzle.position);
                return true;
            }
        }

        return false;
    }
    void ShootProjectile()
    {
        int bulletsPerShotFinal = data.shootsPerFire;// shootType == WeaponShootType.Charge ? Mathf.CeilToInt(currentCharge * bulletsPerShot) : bulletsPerShot;

        // spawn all bullets with random direction
        for (int i = 0; i < bulletsPerShotFinal; i++)
        {
            Vector3 shotDirection = GetShotDirectionWithinSpread(weaponMuzzle);
            ProjectileController newProjectile = Instantiate(data.projectilePrefab, weaponMuzzle.position, Quaternion.LookRotation(shotDirection));
            newProjectile.GetComponent<Projectile>().damage *= Mathf.FloorToInt(ModifierHandler.Instance.weaponDamageMod);
            newProjectile.Shoot(this);
        }
        lastTimeShot = Time.time;
    }
    public Vector3 GetShotDirectionWithinSpread(Transform shootTransform)
    {
        float spreadAngleRatio = data.bulletSpread / 180f;
        Vector3 spreadWorldDirection = Vector3.Slerp(shootTransform.forward, UnityEngine.Random.insideUnitSphere, spreadAngleRatio);

        return spreadWorldDirection;
    }
    public virtual void OnHit(Transform obj)
    {
        Debug.LogError("Hit " + obj.name);
    }
}
