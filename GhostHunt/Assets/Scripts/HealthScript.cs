﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class HealthScript : MonoBehaviour
{
    public int maxHP;
    public int currentHP;
    public UnityEvent onDieEvent;
    public int deathScore;
    public GameObject deathParticles;
    public virtual void Awake()
    {
        currentHP = maxHP;
    }
    public virtual void TakeDamage(int amount)
    {
        if (!(this is PlayerHealth))
        {
            for (int i = 0; i < ModifierHandler.Instance.vampiric; i++)
            {
                Player.Instance.health.Heal(Mathf.FloorToInt(amount / 2));
            }
            LevelManager.Instance.stats.damage += amount;
            if (ModifierHandler.Instance.enemyOHKO)
                amount = 9999;
        }
        currentHP -= amount;
        if (currentHP <= 0)
            Die();
    }
    public virtual void Heal(int amount)
    {
        currentHP = Mathf.Min(currentHP + amount, maxHP);
    }
    protected virtual void Die()
    {
        onDieEvent?.Invoke();
        if (deathParticles)
            GameObject.Instantiate(deathParticles, transform.position, Quaternion.identity);
        for (int i = 0; i < ModifierHandler.Instance.enemiesExplode; i++)
        {
            GameObject.Instantiate(ModifierHandler.Instance.explodePrefab, transform.position, Quaternion.identity);
        }


        Destroy(this.gameObject);
        if (deathScore > 0)
            LevelManager.Score(deathScore);
    }
}
