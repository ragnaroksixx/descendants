﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickupVolume : PickUpVolume
{
    public Weapon weapon;
    public override void OnEnter(Collider other)
    {
        WeaponHandler w = other.GetComponent<WeaponHandler>();
        w?.TryEnter(this);
    }
    public override void OnExit(Collider other)
    {
        WeaponHandler w = other.GetComponent<WeaponHandler>();
        w?.TryExit(this);
    }
}

