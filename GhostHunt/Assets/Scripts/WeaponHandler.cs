﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHandler : MonoBehaviour
{
    public static WeaponData equippedWeaponData;
    public static int cacheAmmo;
    public Weapon equipedWeapon;
    public Transform gunHoldRoot;
    public Camera weaponCamera;
    public HUD hud;
    public bool IsEquipped { get => equipedWeapon != null; }
    // Start is called before the first frame update
    void Start()
    {
        if (equippedWeaponData)
        {
            Weapon w = Instantiate(equippedWeaponData.weaponPrefab);
            w.SetWeaponData(equippedWeaponData);
            EquipWeapon(w);
            w.CurrentAmmo = cacheAmmo;
            hud.SetGun(w);
        }
    }
    private void OnDestroy()
    {
        if (equipedWeapon && !LevelManager.RestartingRound)
        {
            cacheAmmo = equipedWeapon.CurrentAmmo;
            equippedWeaponData = equipedWeapon.data;
        }
    }
    public void EquipWeapon(Weapon w)
    {
        if (equipedWeapon)
        {
            DropWeapon();
        }
        w.equipSFX.Play(transform.position);
        triggeredWeapon = null;
        hud.ShowWeaponPickup(null);
        equipedWeapon = w;
        equipedWeapon.OnEquip(this);
        w.transform.SetParent(gunHoldRoot);
        w.transform.localPosition = Vector3.zero;
        w.transform.localRotation = Quaternion.identity;
    }
    public void DropWeapon()
    {
        equipedWeapon.OnDrop();
        equipedWeapon = null;
        equippedWeaponData = null;
    }
    private void Update()
    {
        if (equipedWeapon)
        {
            if (equipedWeapon.CurrentAmmo == 0 && Input.GetMouseButtonDown(0))
            {
                equipedWeapon.emptySFX.Play(equipedWeapon.weaponMuzzle.transform.position);
            }

            switch (equipedWeapon.data.type)
            {
                case WeaponData.ShootType.Manual:
                    if (Input.GetMouseButtonDown(0))
                    {
                        equipedWeapon.Shoot();
                        return;
                    }
                    break;
                case WeaponData.ShootType.Automatic:
                    if (Input.GetMouseButton(0))
                    {
                        equipedWeapon.Shoot();
                        return;
                    }
                    break;
                case WeaponData.ShootType.Charge:
                    break;
                default:
                    break;
            }

            if (Input.GetMouseButtonDown(2))
            {
                hud.SetGun(null);
                equipedWeapon.OnThrow(weaponCamera.transform.forward);
                equippedWeaponData = null;
                equipedWeapon = null;
                return;
            }

        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            PickUp();
        }

    }
    public WeaponPickupVolume triggeredWeapon;
    public void TryEnter(WeaponPickupVolume wpv)
    {
        if (wpv)
        {
            triggeredWeapon = wpv;
            UpdatePickUpUI();
        }
    }
    public void TryExit(WeaponPickupVolume wpv)
    {
        if (wpv == triggeredWeapon)
        {
            triggeredWeapon = null;
            UpdatePickUpUI();
        }
    }
    public ModifierPickUpVolume triggeredModifier;
    public void TryEnter(ModifierPickUpVolume mpv)
    {
        if (mpv)
        {
            triggeredModifier = mpv;
            UpdatePickUpUI();
        }
    }
    public void TryExit(ModifierPickUpVolume mpv)
    {
        if (mpv == triggeredModifier)
        {
            triggeredModifier = null;
            UpdatePickUpUI();
        }
    }
    public void UpdatePickUpUI()
    {
        if (triggeredModifier)
        {
            hud.ShowWeaponPickup(null);
            hud.ShowModPickUp(triggeredModifier);
        }
        else if (triggeredWeapon)
        {
            hud.ShowModPickUp(null);
            hud.ShowWeaponPickup(triggeredWeapon.weapon);
        }
        else
        {
            hud.ShowModPickUp(null);
            hud.ShowWeaponPickup(null);
        }
    }
    public void PickUp()
    {
        if (triggeredModifier)
        {
            triggeredModifier.PickUp();
            Destroy(triggeredModifier.root);
            triggeredModifier = null;
            UpdatePickUpUI();
        }
        else if (triggeredWeapon)
        {
            EquipWeapon(triggeredWeapon.weapon);
        }
    }
}
