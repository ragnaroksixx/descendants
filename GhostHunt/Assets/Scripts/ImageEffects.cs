﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImageEffects : MonoBehaviour
{
    public Image image;
    Color defaultColor;
    public Color flashColor;
    private void Awake()
    {
        defaultColor = image.color;
    }
    public void Flash(Color c)
    {
        image.DOFlash(defaultColor, c);
    }
    public void Flash()
    {
        Flash(flashColor);
    }
}
