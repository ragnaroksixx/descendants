﻿using UnityEngine;
using System.Collections;

public class GrappleObject : MonoBehaviour
{
    public enum PullType
    {
        PLAYER,
        SELF,
        OTHER
    }
    public PullType pullType;
    public virtual bool IgnoreGrapple()
    {
        return false;
    }
    public virtual void OnGrappleAttach(GrappleHandler gh, RaycastHit hit)
    {
        if (pullType == PullType.PLAYER)
        {
            gh.PullPlayer(hit);
        }
    }
    public virtual void OnGrappleDettach(GrappleHandler gh)
    {

    }
    public virtual void OnGrappleHighlight(GrappleHandler gh)
    {

    }
    public virtual void OnGrappleDehighlight(GrappleHandler gh)
    {

    }
}
