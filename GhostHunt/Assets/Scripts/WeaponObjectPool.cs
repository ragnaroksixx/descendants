﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Weapon Sheet")]
public class WeaponObjectPool : ScriptableObject
{
    public WeaponSpawnData[] spawnables;

    public List<WeaponData> Get(int count)
    {
        List<WeaponData> results = new List<WeaponData>();
        List<WeaponSpawnData> possibleStuff = new List<WeaponSpawnData>(spawnables);
        int value = 100 - Random.Range(0, 100);
        WeaponSpawnData s;
        int infiniteCheck = 1000;
        while (results.Count < count)
        {
            WeaponData val = null;
            while (val == null)
            {
                infiniteCheck--;
                s = possibleStuff[Random.Range(0, possibleStuff.Count)];
                if (s.frequency >= value)
                {
                    val = s.obj;
                    possibleStuff.Remove(s);
                    break;
                }
                else if (infiniteCheck < 0)
                {
                    val = possibleStuff[0].obj;
                    possibleStuff.RemoveAt(0);
                }
                if (results.Contains(val))
                    val = null;
            }
            results.Add(val);
        }
        return results;
    }
}
[System.Serializable]
public struct WeaponSpawnData
{
    public WeaponData obj;
    [Range(0, 100)]
    public int frequency;
}
