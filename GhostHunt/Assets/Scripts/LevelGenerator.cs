﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject endingArea, deadEndHoriz, deadEndVert;
    //public List<GameObject> areaPrefabs;
    List<Area> possibleAreas = new List<Area>();
    List<Area> areas = new List<Area>();
    LevelData level = null;
    int AreaCount
    {
        get => areas.Count;
    }
    public IEnumerator SpawnMap(LevelData l)
    {
        level = l;
        possibleAreas = new List<Area>();
        foreach (GameObject obj in l.PossibleRooms)
        {
            possibleAreas.Add(obj.GetComponentInChildren<Area>());
        }
        yield return null;
        yield return SpawnCorePaths();
        yield return CloseAllOpenPaths();
        levelGenDone = true;
    }
    public bool levelGenDone;
    IEnumerator SpawnCorePaths()
    {
        Debug.Log("Starting Core Path");
        Area lastSpawnedRoom;
        //Spawn STart
        lastSpawnedRoom = SpawnRoom(level.startingArea);
        //while count < minRooms
        int deleteCheck = 0;
        int globalDelete = 0;
        while (AreaCount <= level.minRoomsToEnd)
        {
            //Get Random Next Room
            Area ranRoomPrefab = possibleAreas[Random.Range(0, possibleAreas.Count)];
            int pathIndex = ranRoomPrefab.GetRandomOpenPathIndex();
            PathWay lastRoomRandPath = lastSpawnedRoom.GetRandomOpenPath();
            if (RulesCheck(lastSpawnedRoom, lastRoomRandPath, ranRoomPrefab, pathIndex))
            {
                LevelManager.SetLoading(((float)AreaCount / level.minRoomsToEnd) * 0.9f, "Loading Core Path");
                Area spawn = SpawnRoom(ranRoomPrefab);
                lastRoomRandPath.ConnectArea(spawn, spawn.pathWays[pathIndex]);
                lastSpawnedRoom = spawn;
                deleteCheck = 0;
                yield return new WaitForFixedUpdate();
            }
            else
            {
                deleteCheck++;
                if (deleteCheck > 100 && AreaCount > 1)
                {
                    Debug.LogWarning("deleted area");
                    deleteCheck = 0;
                    globalDelete++;
                    areas.RemoveAt(areas.Count - 1);
                    lastSpawnedRoom.DestoryRoom();
                    lastSpawnedRoom = areas[areas.Count - 1];
                    if (globalDelete > 100)
                    {
                        LevelManager.ReloadScene();
                    }
                    yield return null;
                }
            }
        }
        Area end = SpawnRoom(endingArea);
        lastSpawnedRoom.GetRandomHorizontalOpenPath().ConnectArea(end, end.pathWays[0]);
        Debug.Log("Ending Core Path");
        yield return null;
    }
    public bool RulesCheck(Area lastRoom, PathWay lastRoomPath, Area randomRoom, int roomPathIndex)
    {
        bool result;
        PathWay selectedPath = randomRoom.pathWays[roomPathIndex];
        if (lastRoom.disallowedNextPrefabs.Contains(randomRoom.rootObj.gameObject))
            return false;
        if (AreaCount == level.minRoomsToEnd && !randomRoom.HasHorizontalPath(roomPathIndex))
            return false;
        if (lastRoomPath.isVertical && !randomRoom.HasHorizontalPath(roomPathIndex))
            return false;
        result = lastRoomPath.CanConnectRoom(randomRoom, selectedPath);
        return result;

    }
    IEnumerator CloseAllOpenPaths()
    {
        List<Area> remainingAreas = new List<Area>(areas);
        foreach (Area area in remainingAreas)
        {
            List<PathWay> openPaths = area.GetOpenPaths();
            foreach (PathWay path in openPaths)
            {
                Area deadEndInstance = SpawnRoom(level.GetDeadEnd(deadEndHoriz, deadEndVert, path.isVertical));
                path.ConnectArea(deadEndInstance, deadEndInstance.pathWays[0]);
                yield return null;
            }
        }
    }
    Area SpawnRoom(GameObject prefab)
    {
        Debug.Log("Spawned Room " + AreaCount);
        GameObject obj = null;
        // Instantiate room
        obj = Instantiate(prefab);
        obj.transform.parent = this.transform;

        obj.transform.position = Vector3.zero;
        obj.transform.rotation = Quaternion.identity;
        areas.Add(obj.GetComponentInChildren<Area>());
        return obj.GetComponentInChildren<Area>();
    }
    Area SpawnRoom(Area prefab)
    {
        return SpawnRoom(prefab.rootObj.gameObject);
    }

}
