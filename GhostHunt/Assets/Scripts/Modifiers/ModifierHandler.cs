﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Modifier Handler")]
public class ModifierHandler : ScriptableObject
{
    public int maxModifiers;
    public int numAirJumps = 0;
    public float weaponDamageMod = 1;
    public float damageTakenMod = 1;
    public float speedModifer = 1;
    public float throwModifer = 1;
    public bool canHeal;
    public int enemiesExplode;
    public int vampiric;
    public bool canHoldWeapons;
    public bool playerOHKO;
    public bool enemyOHKO;
    public bool noGrappleCooldown;
    public bool infiniteAmmo;
    public bool canShoot;
    public bool allBounce;
    public bool canChangeWeapons = true;

    public static ModifierHandler Instance;
    public static List<Modifier> PositiveModifiers = new List<Modifier>();
    public static List<Modifier> NegativeModifiers = new List<Modifier>();
    public void OnGameStart()
    {
        Instance = this;
        PositiveModifiers = new List<Modifier>();
        NegativeModifiers = new List<Modifier>();
        ResetValues();
    }
    public void ResetValues()
    {
        numAirJumps = 0;
        weaponDamageMod = 1;
        damageTakenMod = 1;
        speedModifer = 1;
        throwModifer = 1;
        canHeal = true;
        enemiesExplode = 0;
        vampiric = 0;
        canHoldWeapons = false;
        playerOHKO = false;
        enemyOHKO = false;
        noGrappleCooldown = false;
        infiniteAmmo = false;
        canChangeWeapons = true;
        canShoot = false;
        HPMod = 1;
        allBounce = false;
    }
    public void SetJump(Modifier m)
    {
        int dir = m.isPositive ? 1 : -1;
        numAirJumps += dir * Mathf.FloorToInt(m.value);
    }
    public void SetSpeed(Modifier m)
    {
        int dir = m.isPositive ? 1 : 1;
        speedModifer *= dir * m.value;
    }
    public void SetDamageDealt(Modifier m)
    {
        int dir = m.isPositive ? 1 : 1;
        weaponDamageMod *= dir * m.value;
    }
    public void SetDamageTaken(Modifier m)
    {
        int dir = m.isPositive ? 1 : 1;
        damageTakenMod *= dir * m.value;
    }
    public void SetThrowPower(Modifier m)
    {
        int dir = m.isPositive ? 1 : 1;
        throwModifer *= dir * m.value;
    }
    public void GrappleCooldown(Modifier m)
    {
        noGrappleCooldown = true;
    }
    public void OHKO_Player(Modifier m)
    {
        playerOHKO = true;
    }
    public void Bounce(Modifier m)
    {
        allBounce = true;
    }

    public void OHKO_Enemies(Modifier m)
    {
        enemyOHKO = true;
    }
    public GameObject explodePrefab;
    public void GruntBDayParty(Modifier m)
    {
        enemiesExplode += 1;
    }
    public void Vampiric(Modifier m)
    {
        vampiric += 1;
    }
    public float HPMod;
    public void MaxHP(Modifier m)
    {
        int dir = m.isPositive ? 1 : 1;
        HPMod *= dir * m.value;
        Player.Instance.health.ModMaxHP(HPMod);
    }
    public void DisableShoot(Modifier m)
    {
        canShoot = true;
    }
    public void InfiniteAmmo(Modifier m)
    {
        infiniteAmmo = true;
    }
    public void UpdateAllVallues()
    {
        ResetValues();
        foreach (Modifier mod in PositiveModifiers)
        {
            mod.OnModifierAdd();
        }
        foreach (Modifier mod in NegativeModifiers)
        {
            mod.OnModifierAdd();
        }
    }
    public static Modifier lastMod = null;
    private void Awake()
    {
        lastMod = null;
    }
    public void OnGameReset()
    {
        PositiveModifiers.Remove(lastMod);
        NegativeModifiers.Remove(lastMod);
        lastMod = null;
    }
    public void AddModifier(Modifier m)
    {
        if (m.isPositive)
        {
            PositiveModifiers.Insert(0, m);
            if (PositiveModifiers.Count > maxModifiers)
            {
                Modifier toRemove = PositiveModifiers[PositiveModifiers.Count - 1];
                PositiveModifiers.RemoveAt(PositiveModifiers.Count - 1);
                toRemove.OnModifierRemove();
            }
        }
        else
        {
            NegativeModifiers.Insert(0, m);
            if (NegativeModifiers.Count > maxModifiers)
            {
                Modifier toRemove = NegativeModifiers[NegativeModifiers.Count - 1];
                NegativeModifiers.RemoveAt(NegativeModifiers.Count - 1);
                toRemove.OnModifierRemove();
            }
        }
        Player.Instance.weaponHandler.hud.UpdateModifiers();
    }
}
