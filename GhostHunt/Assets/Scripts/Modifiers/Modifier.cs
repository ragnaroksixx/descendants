﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Modifier")]
[System.Serializable]
public class Modifier : ScriptableObject
{
    public string displayName;
    [TextArea(5, 20)]
    public string description;
    public bool isPositive;
    public float value;
    public Sprite image;
    public GameObject prefab;
    public OnModChanged OnModSet;
    public void OnModifierAdd()
    {
        OnModSet.Invoke(this);
    }
    public void OnModifierRemove()
    {
        ModifierHandler.Instance.UpdateAllVallues();
    }
}

[System.Serializable]
public class OnModChanged : UnityEvent<Modifier> { };
