﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Modifier Sheet")]
public class ModifierObjectPool : ScriptableObject
{
    public ModifierSpawnData[] positive;
    public ModifierSpawnData[] negative;
    public ModifierSpawnData[] doubleEdged;

    public List<Modifier> Get(int count, bool pos, bool neg, bool complex)
    {
        List<Modifier> results = new List<Modifier>();
        List<ModifierSpawnData> possibleStuff = new List<ModifierSpawnData>();

        if (pos)
            possibleStuff.AddRange(positive);
        if (neg)
            possibleStuff.AddRange(negative);
        if (complex)
            possibleStuff.AddRange(doubleEdged);

        int value = 100 - Random.Range(0, 100);
        ModifierSpawnData s;
        int infiniteCheck = 1000;
        while (results.Count < count)
        {
            Modifier val = null;
            while (val == null)
            {
                infiniteCheck--;
                s = possibleStuff[Random.Range(0, possibleStuff.Count)];
                if (s.frequency >= value)
                {
                    val = s.obj;
                    possibleStuff.Remove(s);
                    break;
                }
                else if (infiniteCheck < 0)
                {
                    val = possibleStuff[0].obj;
                    possibleStuff.RemoveAt(0);
                }
                if (results.Contains(val))
                    val = null;
            }
            results.Add(val);
        }
        return results;
    }
    [System.Serializable]
    public struct ModifierSpawnData
    {
        public Modifier obj;
        [Range(0, 100)]
        public int frequency;
    }
}