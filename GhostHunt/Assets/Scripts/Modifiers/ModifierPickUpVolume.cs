﻿using UnityEngine;
using System.Collections;

public class ModifierPickUpVolume : PickUpVolume
{
    public Modifier data;
    public GameObject root;
    public delegate void OnPickUp();
    public OnPickUp onPickup;
    public override void OnEnter(Collider other)
    {
        WeaponHandler w = other.GetComponent<WeaponHandler>();
        w?.TryEnter(this);
    }
    public override void OnExit(Collider other)
    {
        WeaponHandler w = other.GetComponent<WeaponHandler>();
        w?.TryExit(this);
    }
    public void SetModifier(Modifier m)
    {
        data = m;
    }
    bool once = true;
    public void PickUp()
    {
        if (once)
        {
            pickUpSFX.Play(transform.position);
            onPickup?.Invoke();
            once = false;
            LevelManager.Instance.stats.mods++;
            LevelManager.Score(500);
            ModifierHandler.lastMod = data;
            ModifierHandler.Instance.AddModifier(data);
            data.OnModifierAdd();
        }
    }
}
