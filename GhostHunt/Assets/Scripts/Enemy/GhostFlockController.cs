﻿using UnityEngine;
using System.Collections;

public class GhostFlockController : Flock
{
    public float spawnTimer, spawnRate;
    public int batchCount;
    public int maxNumGhost;
    float timeTrack;
    bool spawnLock;
    public override IEnumerator Start()
    {
        spawnLock = true;
        while(!LevelManager.zoneStarted)
        {
            yield return null;
        }
        yield return base.Start();
        spawnLock = false;
        timeTrack = Time.time + spawnTimer;
    }
    public override void Update()
    {
        base.Update();
        if (!spawnLock && Time.time > timeTrack)
        {
            int toSpawn = batchCount;
            if (toSpawn + Agents.Count > maxNumGhost)
            {
                toSpawn = maxNumGhost - Agents.Count;
            }
            StartCoroutine(SpawnMultiple(toSpawn));
        }
    }
    public IEnumerator SpawnMultiple(int count)
    {
        spawnLock = true;
        spawnSFX.Play();
        for (int i = 0; i < count; i++)
        {
            Spawn(count);
            yield return new WaitForSeconds(spawnRate);
        }
        spawnLock = false;
        timeTrack = Time.time + spawnTimer;
    }
}
