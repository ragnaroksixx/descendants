﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
public class GhostFlockAgent : FlockAgent
{
    public HitBox hitBox;
    public Hurtbox hurbox;
    //public MeshRenderer meshRend;
    //Material mat;
    public float fadeOutTime = 1;
    public float fadeInTime = 1;
    public float maxDistance = 10;
    public float reAppearanceRange = 5;
    CoroutineHandler coroutineRunner, recoilCoroutine;
    public float recoilSpeed = 1;
    public SpriteRenderer sprite;
    public CustomAudioClip dieSFX;

    private void Awake()
    {
        hitBox.enabled = false;
        //mat = meshRend.material;
        coroutineRunner = new CoroutineHandler(this);
        recoilCoroutine = new CoroutineHandler(this);
        hurbox.onHitEvents.AddListener(Recoil);
    }
    void FixedUpdate()
    {
        if (!coroutineRunner.IsRunning)
        {
            if (Vector3.Distance(transform.position, Player.Instance.transform.position) > maxDistance)
            {
                coroutineRunner.StartCoroutine(MoveToPlayer());
            }
        }
    }
    IEnumerator MoveToPlayer()
    {
        hitBox.enabled = false;
        hurbox.enabled = false;
        sprite.DOFade(0, fadeOutTime);
        yield return new WaitForSeconds(fadeOutTime);
        yield return new WaitForSeconds(2);
        Vector3 pos = Player.Instance.aiFWDTarget.transform.position + Player.Instance.transform.forward + (Random.insideUnitSphere * reAppearanceRange);
        transform.position = pos;
        sprite.DOFade(1, fadeInTime);
        hurbox.enabled = true;
        yield return new WaitForSeconds(fadeInTime);
        hitBox.enabled = true;
    }
    IEnumerator FadeIn()
    {
        hitBox.enabled = false;
        hurbox.enabled = false;
        sprite.DOFade(0, 0);
        yield return new WaitForSeconds(1);
        sprite.DOFade(1, fadeInTime);
        hurbox.enabled = true;
        yield return new WaitForSeconds(fadeInTime);
        hitBox.enabled = true;
    }
    public void Recoil()
    {
        recoilCoroutine.StartCoroutine(RecoilCo(-Player.Instance.transform.forward));
    }
    public IEnumerator RecoilCo(Vector3 dir)
    {
        float time = 1;
        while (time > 0)
        {
            yield return new WaitForEndOfFrame();
            time -= Time.deltaTime;
            transform.position += dir * recoilSpeed * Time.deltaTime;
        }
    }
    public override void Move(Vector3 velocity)
    {
        Vector3 ori = transform.position - Player.Instance.aiFWDTarget.position;
        ori.y = 0;
        sprite.transform.forward = ori;
        if (recoilCoroutine.IsRunning) return;
        base.Move(velocity);
    }
    public override void Initialize(Flock flock)
    {
        base.Initialize(flock);
        coroutineRunner.StartCoroutine(FadeIn());
    }
    public void OnKill()
    {
        dieSFX.Play(transform.position);
        LevelManager.Kill();
    }
}
