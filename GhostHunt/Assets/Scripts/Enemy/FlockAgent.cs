﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class FlockAgent : MonoBehaviour
{
    public float moveVariance;
    protected float moveVar;
    Flock agentFlock;
    public Flock AgentFlock { get { return agentFlock; } }

    Collider agentCollider;
    public Collider AgentCollider { get { return agentCollider; } }

    // Start is called before the first frame update
    void Start()
    {
        agentCollider = GetComponent<Collider>();
    }

    public virtual void Initialize(Flock flock)
    {
        agentFlock = flock;
        moveVar = 1 + Random.Range(-moveVariance, moveVariance);
    }

    public virtual void Move(Vector3 velocity)
    {
        velocity *= moveVar;
        transform.up = velocity;
        transform.position += velocity * Time.deltaTime;
    }
    private void OnDestroy()
    {
        AgentFlock.Agents.Remove(this);
    }
}
