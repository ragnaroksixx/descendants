﻿using UnityEngine;
using System.Collections;
using CMF;

public class MeleeAI : BasicAI
{
    public HitBox hb;
    public override void Awake()
    {
        hb.gameObject.SetActive(false);
        base.Awake();
    }
    public override void Attack()
    {
        base.Attack();
        hb.gameObject.SetActive(true);
    }
    public override void AttackEnd()
    {
        base.AttackEnd();
        hb.gameObject.SetActive(false);
    }
}

