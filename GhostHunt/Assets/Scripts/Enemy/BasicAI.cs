﻿using UnityEngine;
using System.Collections;
using CMF;

public class BasicAI : MonoBehaviour
{
    public float detectionRange;
    Vector3 spawnPoint;
    protected Transform targetPos = null;
    public Rigidbody rBody;
    public float moveSpeed;
    public Mover mover;
    AIState aiState;
    public Animator anim;
    public CustomAudioClip spottedSFX, deathSFX;
    public bool ignoreY = true;
    public bool ignoreGravity = false;
    public GameObject seperateFromSystem;
    public enum AIState
    {
        IDLE,
        PURSUE,
        RETREAT,
        ATTACK
    }
    public virtual void Awake()
    {
        if (seperateFromSystem)
        {
            seperateFromSystem.transform.SetParent(null, true);
        }
        spawnPoint = transform.position;
    }
    private void Update()
    {
        Vector3 targetPosition;
        float distance;
        switch (aiState)
        {
            case AIState.IDLE:
                break;
            case AIState.PURSUE:
                targetPosition = targetPos.position;
                if (ignoreY)
                    targetPosition.y = transform.position.y;
                distance = Vector3.Distance(targetPosition, transform.position);
                if (distance >= detectionRange)
                {
                    SetState(AIState.RETREAT);
                    return;
                }
                else if (distance <= attackRange)
                {
                    SetState(AIState.ATTACK);
                    return;
                }
                MoveTowards(targetPosition);
                break;
            case AIState.RETREAT:
                targetPosition = spawnPoint;
                if (ignoreY)
                    targetPosition.y = transform.position.y;
                distance = Vector3.Distance(targetPosition, transform.position);
                if (distance <= 3)
                {
                    SetState(AIState.IDLE);
                    return;
                }

                MoveTowards(targetPosition);
                break;
            case AIState.ATTACK:
                break;
            default:
                break;
        }
    }
    public void MoveTowards(Vector3 targetPos)
    {
        Vector3 fwd = targetPos - transform.position;
        transform.forward = fwd;
        fwd *= moveSpeed;
        if (mover.IsGrounded() || ignoreGravity)
        {

        }
        else
        {
            fwd += Physics.gravity;
        }
        if (moveSpeed != 0)
            mover.SetVelocity(fwd);
        //rBody.MovePosition(Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * moveSpeed));
    }
    private void OnTriggerEnter(Collider other)
    {
        if ((aiState == AIState.IDLE || aiState == AIState.RETREAT) && other.tag == "Player")
        {
            spottedSFX.Play();
            targetPos = other.transform;
            SetState(AIState.PURSUE);
        }
    }
    public void SetState(AIState state)
    {
        aiState = state;
        switch (aiState)
        {
            case AIState.IDLE:
                if (anim)
                    anim.SetBool("isRunning", false);
                mover.SetVelocity(Vector3.zero);
                break;
            case AIState.PURSUE:
                if (anim)
                    anim.SetBool("isRunning", true);
                break;
            case AIState.RETREAT:
                if (anim)
                    anim.SetBool("isRunning", true);
                break;
            case AIState.ATTACK:
                if (anim)
                {
                    anim.SetBool("isRunning", false);
                    anim.SetTrigger("shoot");
                }
                mover.SetVelocity(Vector3.zero);
                StartCoroutine(AttackAction());
                break;
            default:
                break;
        }
    }
    public void DetectOnHit()
    {
        if ((aiState == AIState.IDLE || aiState == AIState.RETREAT))
        {
            spottedSFX.Play();
            targetPos = Player.Instance.transform;
            SetState(AIState.PURSUE);
        }
    }
    public float attackTime;
    public float animTime;
    public float attackRange;
    IEnumerator AttackAction()
    {
        mover.SetVelocity(Physics.gravity);
        yield return new WaitForSeconds(animTime);
        Attack();
        yield return new WaitForSeconds(attackTime);
        Vector3 targetPosition;
        AttackEnd();
        targetPosition = targetPos.position;
        if (ignoreY)
            targetPosition.y = transform.position.y;
        else
            targetPosition.y += 0.5f;
        float distance = Vector3.Distance(targetPosition, transform.position);
        if (distance >= detectionRange)
        {
            SetState(AIState.RETREAT);
        }
        else if (distance <= attackRange)
        {
            Vector3 fwd = targetPosition - transform.position;
            transform.forward = Vector3.Slerp(transform.forward, fwd, smoothRotSpeed);
            SetState(AIState.ATTACK);
        }
        else
        {
            SetState(AIState.PURSUE);
        }
    }
    public float smoothRotSpeed = 1;
    public virtual void Attack()
    {

    }
    public virtual void AttackEnd()
    {

    }
    public void OnDie()
    {
        deathSFX.Play();
    }
}

