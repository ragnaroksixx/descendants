﻿using UnityEngine;
using System.Collections;
using CMF;

public class ShooterAI : BasicAI
{
    public WeaponData data;
    public Transform weaponMuzzle;
    public Transform weaponMuzzle2;
    public bool aimMuzzle = false;
    public bool shootIfLineOfSight;
    public override void Attack()
    {
        base.Attack();
        ShootProjectile();
    }
    public override void AttackEnd()
    {
        if (aimMuzzle)
        {
            Vector3 fwd = targetPos.position - transform.position;
            weaponMuzzle.forward = fwd;
            if (weaponMuzzle2)
                weaponMuzzle2.forward = fwd;
        }
        base.AttackEnd();
    }
    void ShootProjectile()
    {
        Shoot(weaponMuzzle);
        if (weaponMuzzle2)
            Shoot(weaponMuzzle2);
    }
    void Shoot(Transform t)
    {
        int bulletsPerShotFinal = data.shootsPerFire;// shootType == WeaponShootType.Charge ? Mathf.CeilToInt(currentCharge * bulletsPerShot) : bulletsPerShot;

        data.fireSFX.Play(t.position);
        // spawn all bullets with random direction
        for (int i = 0; i < bulletsPerShotFinal; i++)
        {
            Vector3 shotDirection = GetShotDirectionWithinSpread(t);
            ProjectileController newProjectile = Instantiate(data.projectilePrefab, t.position, Quaternion.LookRotation(shotDirection));
            newProjectile.Shoot(this);
        }
    }
    public Vector3 GetShotDirectionWithinSpread(Transform shootTransform)
    {
        float spreadAngleRatio = data.bulletSpread / 180f;
        Vector3 spreadWorldDirection = Vector3.Slerp(shootTransform.forward, UnityEngine.Random.insideUnitSphere, spreadAngleRatio);

        return spreadWorldDirection;
    }
}

