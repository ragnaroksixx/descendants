﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Filter/Player Filter")]
public class PlayerFilter : ContextFilter
{
    public override List<Transform> Filter(FlockAgent agent, List<Transform> original)
    {
        List<Transform> filtered = new List<Transform>();
        if (Player.Instance)
            filtered.Add(Player.Instance.transform);
        return filtered;
    }
}

