﻿using UnityEngine;
using System.Collections;

public class SpriteEffects : MonoBehaviour
{
    public SpriteRenderer sr;
    public Color flashColor;
    Color defaultColor;
    private void Awake()
    {
        if (!sr)
            sr = GetComponent<SpriteRenderer>();
        defaultColor = sr.color;
    }
    public void Flash(Color c)
    {
        sr.DOFlash(defaultColor, c);
    }
    public void Flash()
    {
        Flash(flashColor);
    }
}
