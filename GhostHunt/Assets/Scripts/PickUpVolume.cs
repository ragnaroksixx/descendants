﻿using UnityEngine;
using System.Collections;

public abstract class PickUpVolume : MonoBehaviour
{
    public CustomAudioClip pickUpSFX;
    public abstract void OnEnter(Collider other);
    public abstract void OnExit(Collider other);
    private void OnTriggerEnter(Collider other)
    {
        OnEnter(other);
    }
    private void OnTriggerExit(Collider other)
    {
        OnExit(other);
    }
}
