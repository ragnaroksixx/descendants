﻿using UnityEngine;
using System.Collections;

public class ObjectSpawning : MonoBehaviour
{
    public SpawnData[] spawnables;
    public bool spawnOnStart;
    public bool destroyAfterSpawn;
    // Use this for initialization
    public virtual IEnumerator Start()
    {
        while (!LevelManager.Instance.levelGen.levelGenDone)
            yield return null;
        if (spawnOnStart)
            Spawn();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public virtual void Spawn()
    {
        int value = 100 - Random.Range(0, 100);
        GameObject val = null;
        SpawnData s;
        int infiniteCheck = 1000;
        while (val == null)
        {
            infiniteCheck--;
            s = spawnables[Random.Range(0, spawnables.Length)];
            if (s.frequency >= value)
            {
                val = s.obj;
                break;
            }
            else if (infiniteCheck < 0)
            {
                val = spawnables[0].obj;
            }
        }
        if (val)
        {
            GameObject.Instantiate(val, transform.position, Quaternion.identity);
        }
        if (destroyAfterSpawn)
            Destroy(this.gameObject);
    }
}
[System.Serializable]
public struct SpawnData
{
    public GameObject obj;
    [Range(0, 100)]
    public int frequency;

    public SpawnData(GameObject obj, int frequency)
    {
        this.obj = obj;
        this.frequency = frequency;
    }
}
