﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GunProjectile : Projectile
{
    protected override void OnShoot()
    {
        base.OnShoot();
        WeaponData wData = LevelManager.Instance.allWeapons.Get(1)[0];
        Weapon w = Instantiate(wData.weaponPrefab, transform.position, Quaternion.identity);
        w.SetWeaponData(wData);
        w.transform.localScale = Vector3.one;
        w.OnThrow(transform.forward);
        Destroy(this.gameObject);
    }
}

