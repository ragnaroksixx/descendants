﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class TimeHandler : MonoBehaviour
{
    CoroutineHandler coroutine;
    public static TimeHandler Instance;
    Tween t;
    public CanvasGroup pauseScreen;
    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start()
    {
        coroutine = new CoroutineHandler(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelManager.Instance.levelGen.levelGenDone)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (isPaused)
                    UnPause();
                else
                    Pause();
            }
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            if (AudioSystem.Instance.MASTERvolumePercentage == 0)
            {
                AudioSystem.SetMasterPercentage(25);
            }
            else
            {
                AudioSystem.SetMasterPercentage(0);
            }
        }
        if (isPaused && Input.GetKeyDown(KeyCode.R))
        {
            LevelManager.ReloadScene();
        }
        if (isPaused && Input.GetKeyDown(KeyCode.Q))
        {
            LevelManager.MainMenu();
        }
    }
    bool isPaused = false;
    public void Pause()
    {
        coroutine.StopCoroutine();
        Time.timeScale = 0.0000000001f;
        isPaused = true;
        pauseScreen.alpha = 1;
    }
    public void UnPause()
    {
        isPaused = false;
        Time.timeScale = 1;
        pauseScreen.alpha = 0;
    }
    public void SlowTime(float time)
    {
        coroutine.StartCoroutine(SlowMo(time), OnInterrupt);
    }
    IEnumerator SlowMo(float time)
    {
        float declTime = time / 2;
        t = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 0.75f, declTime);
        yield return new WaitForSeconds(time / 2);
        t = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1, declTime);
        Time.timeScale = 1;
    }
    void OnInterrupt()
    {
        t?.Kill();
        Time.timeScale = 1;
    }
}
