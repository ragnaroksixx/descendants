﻿using UnityEngine;
using System.Collections;

public class ExitVolume : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.root.gameObject.CompareTag("Player"))
        {
            StartNextLevel();
        }
    }
    void StartNextLevel()
    {
        LevelManager.EndLevel();
    }

}
