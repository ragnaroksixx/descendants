﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StartRoom : MonoBehaviour
{
    public Sliding_Door_Script door;
    public WorldTextController choose, your, weapon, power;

    public List<Transform> weaponSpawns;
    public List<Transform> modifierSpawns;
    List<ModifierPickUpVolume> mPickUps = new List<ModifierPickUpVolume>();

    // Use this for initialization
    IEnumerator Start()
    {
        door.isLocked = true;
        while (!LevelManager.Instance.levelGen.levelGenDone)
        {
            yield return null;
        }
        choose.Play();
        yield return new WaitForSeconds(0.6f);
        your.Play();
        yield return new WaitForSeconds(0.6f);
        weapon.Play();
        power.Play();
        List<WeaponData> possibleWeapons = LevelManager.Instance.allWeapons.Get(3);
        for (int i = 0; i < weaponSpawns.Count; i++)
        {
            WeaponData wData = possibleWeapons[i];
            Weapon w = Instantiate(wData.weaponPrefab, weaponSpawns[i].transform.position, weaponSpawns[i].transform.rotation);
            w.SetWeaponData(wData);
            w.transform.localScale = Vector3.one;
        }
        List<Modifier> possibleModifiers = LevelManager.Instance.allMods.Get(3, LevelManager.levelIndex % 2 == 0
            , LevelManager.levelIndex % 2 == 1, false);
        for (int i = 0; i < modifierSpawns.Count; i++)
        {
            Modifier mData = possibleModifiers[Random.Range(0, possibleModifiers.Count)];
            if (mData)
            {
                possibleModifiers.Remove(mData);
                ModifierPickUpVolume m = Instantiate(mData.prefab, modifierSpawns[i].transform.position, modifierSpawns[i].transform.rotation)
                    .GetComponentInChildren<ModifierPickUpVolume>();
                m.onPickup += OnModifierCollected;
                mPickUps.Add(m);
                m.SetModifier(mData);
                m.transform.localScale = Vector3.one;
            }
        }
    }

    public void OnModifierCollected()
    {
        foreach (ModifierPickUpVolume item in mPickUps)
        {
            if (item)
            {
                Destroy(item.root);
            }
        }
        door.isLocked = false;
        door.SlideDoors(true);
    }
}
