﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public static Player Instance;
    public Transform aiFWDTarget;
    public WeaponHandler weaponHandler;
    public PlayerHealth health;
    public static int cacheHP = -1, cacheARMOR = -1;
    private void Awake()
    {
        Instance = this;
    }
    private void OnDestroy()
    {
        Instance = null;
    }
}
