﻿using UnityEngine;
using System.Collections;
using TMPro;
using Coffee.UIExtensions;
using DG.Tweening;

public class WorldTextController : MonoBehaviour
{
    public TMP_Text textObj;
    [TextArea(5, 20)]
    public string text;
    public UIDissolve dissolveEffect;
    public float fadeInTime, remainTime, fadeOutTime;
    public bool autoPlay;
    // Use this for initialization
    void Start()
    {
        dissolveEffect.effectFactor = 1;
        textObj.text = text;
        if (autoPlay)
            Play();
    }
    public void Play()
    {
        StartCoroutine(PlayCo());
    }
    IEnumerator PlayCo()
    {
        DOTween.To(() => dissolveEffect.effectFactor, x => dissolveEffect.effectFactor = x, 0, fadeInTime);
        yield return new WaitForSeconds(fadeInTime + remainTime);
        DOTween.To(() => dissolveEffect.effectFactor, x => dissolveEffect.effectFactor = x, 1, fadeOutTime);
        yield return new WaitForSeconds(fadeOutTime);
        Destroy(this.gameObject);
    }
}
