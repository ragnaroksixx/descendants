﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerHealth : HealthScript
{
    bool isDead;
    public int armor;
    public int maxArmor;
    public CustomAudioClip damageSFX, deathSFX;
    public int realMaxHP;
    public override void Awake()
    {
        if (Player.cacheARMOR > 0)
            ArmorUp(Player.cacheARMOR);
        if (Player.cacheHP > 0)
            currentHP = Player.cacheHP;
        else
            base.Awake();

    }
    public void ModMaxHP(float val)
    {
        maxHP = Mathf.FloorToInt(realMaxHP * val);
        currentHP = Mathf.Min(maxHP, currentHP);
        Player.Instance.weaponHandler.hud.UpdateHealth();
    }
    private void OnDestroy()
    {
        if (!LevelManager.RestartingRound)
        {
            Player.cacheARMOR = armor;
            Player.cacheHP = currentHP;
        }
    }
    float iFrames = .2f;
    float iFramesTrack;
    public override void TakeDamage(int amount)
    {
        if (isDead) return;
        if (Time.time < iFramesTrack) return;
        iFramesTrack = Time.time + iFrames;
        damageSFX.Play();
        if (ModifierHandler.Instance.playerOHKO)
            amount = 9999;
        amount = LoseArmor(amount);
        base.TakeDamage(amount);
        Player.Instance.weaponHandler.hud.UpdateHealth();
    }
    public override void Heal(int amount)
    {
        base.Heal(amount);
        Player.Instance.weaponHandler.hud.UpdateHealth();
    }
    protected override void Die()
    {
        if (isDead) return;
        deathSFX.Play();
        Player.cacheARMOR = -1;
        Player.cacheHP = -1;
        LevelManager.GameOver();
        isDead = true;
    }
    public void ArmorUp(int val)
    {
        armor = Mathf.Min(maxArmor, armor + val);
        Player.Instance.weaponHandler.hud.UpdateHealth();
    }
    public int LoseArmor(int val)
    {
        armor -= val;
        if (armor < 0)
        {
            int rem = -armor;
            armor = 0;
            return rem;
        }
        return 0;
    }
}

