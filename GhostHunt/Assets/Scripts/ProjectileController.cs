﻿using UnityEngine;
using UnityEngine.Events;

public class ProjectileController : MonoBehaviour
{
    public GameObject owner { get; private set; }
    public Vector3 initialPosition { get; private set; }
    public Vector3 initialDirection { get; private set; }
    public Vector3 inheritedMuzzleVelocity { get; private set; }
    public float initialCharge { get; private set; }

    public UnityAction onShoot;

    public void Shoot(Weapon weapon)
    {
        owner = weapon.Owner.transform.root.gameObject;
        initialPosition = transform.position;
        initialDirection = transform.forward;
        inheritedMuzzleVelocity = weapon.muzzleWorldVelocity;
        //initialCharge = controller.currentCharge;

        if (onShoot != null)
        {
            onShoot.Invoke();
        }
    }
    public void Shoot(BasicAI enemy)
    {
        owner = enemy.gameObject;
        initialPosition = transform.position;
        initialDirection = transform.forward;
        inheritedMuzzleVelocity = enemy.rBody.velocity;
        //initialCharge = controller.currentCharge;

        if (onShoot != null)
        {
            onShoot.Invoke();
        }
    }
}