﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponSpawner : ObjectSpawning
{
    public override void Spawn()
    {
        WeaponData wData = LevelManager.Instance.allWeapons.Get(1)[0];
        Weapon w = Instantiate(wData.weaponPrefab, transform.position, Quaternion.identity);
        w.SetWeaponData(wData);
        w.transform.localScale = Vector3.one;
        if (destroyAfterSpawn)
            Destroy(this.gameObject);
    }
}

