﻿using CMF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHandler : MonoBehaviour
{
    public HUD hud;
    public AdvancedWalkerController cont;
    public LineRenderer lr;

    public LayerMask whatIsGrappleable;
    public Transform camera, gunTip;
    public float launchAccelrationFWD = 1.75f;
    public float launchAccelrationUP = 0.2f;
    public float MaxDistanceModifier = 15;
    public float MinDistanceModifier = 5;
    bool inUse;

    //Grapple key variables;
    bool grappleKeyWasLetGo = false;
    bool grappleKeyIsPressed = false;

    Vector3 currentGrapplePosition;
    public float successfulCooldown;
    public float unSuccessfulCooldown;
    float cooldownTrack;
    public float grappleDistance = 30;
    CoroutineHandler grappleCoroutine;
    // Start is called before the first frame update
    void Start()
    {
        grappleCoroutine = new CoroutineHandler(this);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        HandleGrappleKeyInput();
        HandleGrapple();
    }
    void FixedUpdate()
    {

    }
    public CustomAudioClip grappleSFX;
    public void HandleGrapple()
    {
        if ((Input.GetKeyDown(KeyCode.Mouse1)) && !inUse && canUse)
        {
            grappleSFX.Play(transform.position);
            grappleCoroutine.StartCoroutine(Use());
        }
    }
    public bool canUse
    {
        get => ModifierHandler.Instance.noGrappleCooldown || Time.time > cooldownTrack && !grappleCoroutine.IsRunning;
    }
    //Handle jump booleans for later use in FixedUpdate;
    void HandleGrappleKeyInput()
    {
        grappleKeyIsPressed = Input.GetKey(KeyCode.Mouse1);
        grappleKeyWasLetGo = Input.GetKeyUp(KeyCode.Mouse1);
        /*
        bool _newgrappleKeyPressedState = Input.GetKey(KeyCode.Mouse1);

        if (grappleKeyIsPressed == false && _newgrappleKeyPressedState == true)
        {
            grappleKeyWasLetGo = false;
        }
        else if (grappleKeyIsPressed == true && _newgrappleKeyPressedState == false)
        {
            grappleKeyWasLetGo = true;
        }

        grappleKeyIsPressed = _newgrappleKeyPressedState;
        */
    }
    void LateUpdate()
    {
        if (!grappleCoroutine.IsRunning)
        {
            DrawRope();
            if (inUse)
            {
                if (grappleKeyWasLetGo || cont.currentControllerState != AdvancedWalkerController.ControllerState.Grappling)
                {
                    cont.currentControllerState = AdvancedWalkerController.ControllerState.Rising;
                    StopGrapple();
                }
            }
        }
    }
    public void PullPlayer(RaycastHit hit)
    {
        lr.positionCount = 2;
        Vector3 grappleLastLaunchVector;
        grappleLastLaunchVector = (-camera.transform.position + hit.point).normalized * launchAccelrationFWD;
        //if (camera.transform.position.y <= hitData.point.y+5)
        grappleLastLaunchVector += camera.up * launchAccelrationUP;
        float distMod = Mathf.Min(MaxDistanceModifier, hit.distance);
        distMod = Mathf.Max(MinDistanceModifier, distMod);
        grappleLastLaunchVector *= distMod;

        cont.OnGrappleStart(grappleLastLaunchVector);
        currentGrapplePosition = gunTip.transform.position;
        hud.ClearGrappleUI();
        inUse = true;
    }
    void StopGrapple()
    {
        inUse = false;
        lr.positionCount = 0;
        cooldownTrack = Time.time + successfulCooldown;
        hud.CoolDownGrapple(successfulCooldown);
    }
    void DrawRope()
    {
        //If not grappling, don't draw rope
        if (!inUse) return;

        currentGrapplePosition = Vector3.Lerp(currentGrapplePosition, GetGrapplePoint(), Time.deltaTime * 8f);

        lr.SetPosition(0, gunTip.position);
        lr.SetPosition(1, currentGrapplePosition);
    }
    public IEnumerator Use()
    {
        lr.positionCount = 2;
        currentGrapplePosition = gunTip.transform.position;
        //inUse = true;
        GrappleObject gObject;
        RaycastHit hit;
        bool didHit = RaycastGrapple(out gObject, out hit);
        float animTime = .15f;
        float animTimeTrack = 0;
        while (gObject && animTimeTrack < animTime)
        {
            animTimeTrack += Time.deltaTime;
            currentGrapplePosition = Vector3.Lerp(currentGrapplePosition, GetGrapplePoint(), animTimeTrack / animTime);

            lr.SetPosition(0, gunTip.position);
            lr.SetPosition(1, currentGrapplePosition);
            yield return null;
        }
        if (didHit && (gObject || hit.collider != null))
        {
            if (gObject && !gObject.IgnoreGrapple())
            {
                gObject.OnGrappleAttach(this, hit);
                lr.positionCount = 0;
            }
            else
            {
                PullPlayer(hit);
            }
        }
        else
        {
            cooldownTrack = Time.time + unSuccessfulCooldown;
            hud.GrappleNoHit();
            hud.CoolDownGrapple(unSuccessfulCooldown);
            hitPoint = (camera.forward * grappleDistance) + camera.position;
            lr.positionCount = 0;
        }


    }
    bool RaycastGrapple(out GrappleObject gData, out RaycastHit hit)
    {
        hit = default;
        bool validHit = false;
        gData = null;
        RaycastHit[] hits = Physics.SphereCastAll(camera.position, 1.0f, camera.forward, grappleDistance, whatIsGrappleable);
        if (hits.Length > 0)
        {
            foreach (RaycastHit h in hits)
            {
                gData = h.collider.gameObject.transform.root.GetComponentInChildren<GrappleObject>();
                if (gData && !gData.IgnoreGrapple())
                {
                    validHit = true;
                    hitPoint = h.point;
                    break;
                }
                gData = null;
            }
        }
        if (gData == null)
        {
            if (Physics.Raycast(camera.position, camera.forward, out hit, grappleDistance, whatIsGrappleable))
            {
                hitPoint = hit.point;
                validHit = true;
                gData = hit.collider.gameObject.transform.root.GetComponentInChildren<GrappleObject>();
            }
            else
            {
                hitPoint = (camera.forward * 3) + camera.position;
            }
        }
        return validHit;
    }
    public bool IsGrappling()
    {
        return inUse;
    }

    Vector3 hitPoint;
    public Vector3 GetGrapplePoint()
    {
        return hitPoint;
    }
}
