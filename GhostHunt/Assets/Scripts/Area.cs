﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(BoxCollider))]
public class Area : MonoBehaviour
{
    public string id = "undefined";
    public List<PathWay> pathWays;
    public BoxCollider boundsCol;
    public Transform rootObj;
    public List<GameObject> disallowedNextPrefabs;
    public int maxNumRooms = -1;
    private void OnValidate()
    {
        boundsCol = GetComponent<BoxCollider>();
        rootObj = transform.root;
        pathWays = new List<PathWay>(rootObj.GetComponentsInChildren<PathWay>());
    }
    // Start is called before the first frame update
    void Start()
    {

    }
    public void DestoryRoom()
    {
        foreach (PathWay path in pathWays)
        {
            if (path.HasConnectedRoom)
            {
                path.Unlink();
            }
        }
        Destroy(rootObj.gameObject);
    }

    public PathWay GetRandomOpenPath()
    {
        List<PathWay> freeRooms = GetOpenPaths();
        if (freeRooms.Count == 0)
            return null;

        return freeRooms[UnityEngine.Random.Range(0, freeRooms.Count)];
    }
    public PathWay GetRandomHorizontalOpenPath()
    {
        foreach (PathWay path in pathWays)
        {
            if (!path.HasConnectedRoom && !path.isVertical)
                return path;
        }
        return null;
    }
    public int GetRandomOpenHorizontalPathIndex()
    {
        PathWay result = GetRandomHorizontalOpenPath();
        if (result == null)
            return -1;
        return pathWays.IndexOf(result);
    }
    public int GetRandomOpenPathIndex()
    {
        PathWay result = GetRandomOpenPath();
        if (result == null)
            return -1;
        return pathWays.IndexOf(result);
    }
    public List<PathWay> GetOpenPaths()
    {
        return pathWays.FindAll(IsFree);
    }
    private bool IsFree(PathWay obj)
    {
        return !obj.HasConnectedRoom;
    }
    public bool HasHorizontalPath(params int[] ignoreIndicies)
    {
        List<int> indx = new List<int>(ignoreIndicies);
        for (int i = 0; i < pathWays.Count; i++)
        {
            if (indx.Contains(i)) continue;
            if (!pathWays[i].isVertical)
                return true;
        }
        return false;
    }
}
