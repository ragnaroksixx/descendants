﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MaterialEffects : MonoBehaviour
{
    Material mat;
    Color defaultColor;
    public Renderer meshRender;
    public Color flashColor;
    private void Awake()
    {
        mat = meshRender.material;
        defaultColor = mat.color;
    }
    public void Flash(Color c)
    {
        mat.DOFlash(defaultColor, c);
    }
    public void EFlash(Color c)
    {
        mat.DOFlash(defaultColor, c);
    }
    public void Flash()
    {
        Flash(flashColor);
    }
    public void EmmisionFlash()
    {
        mat.DOeFlash(new Color(), Color.white, flashColor, numLoops: 1);
    }
    public void Squash()
    {
    }
}
