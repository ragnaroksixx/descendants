﻿using UnityEngine;
using System.Collections;

public class ThrowHitBox : HitBox
{
    public Weapon weapon;
    protected override void OnHit(Hurtbox hb)
    {
        if (!enabled) return;
        if (hb == null) return;
        if (Player.Instance != null &&
            hb.transform.root == Player.Instance.transform.root) return;
        Vector3 dirToPlayer = Player.Instance.aiFWDTarget.position
            - transform.position;
        dirToPlayer.Normalize();
        float mag = weapon.RBody.velocity.magnitude;
        TimeHandler.Instance.SlowTime(1);
        weapon.RBody.velocity = (dirToPlayer * 10);
        base.OnHit(hb);
    }
}

