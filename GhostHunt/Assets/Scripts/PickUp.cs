﻿using UnityEngine;
using System.Collections;

public class PickUp : PickUpVolume
{
    public int amount;
    public enum PICKUPTYPE
    {
        HP,
        AMMO,
        ARMOR,
        SPECIAL
    }
    public PICKUPTYPE pType;

    public override void OnEnter(Collider other)
    {
        if (!other.GetComponent<WeaponHandler>())
            return;
        switch (pType)
        {
            case PICKUPTYPE.HP:
                if (pickUpSFX)
                    pickUpSFX.Play(transform.position);
                Player.Instance.health.Heal(amount);
                break;
            case PICKUPTYPE.AMMO:
                if (!Player.Instance.weaponHandler.IsEquipped)
                    return;
                if (pickUpSFX)
                    pickUpSFX.Play(transform.position);
                Player.Instance.weaponHandler.equipedWeapon.Ammo(amount);
                break;
            case PICKUPTYPE.ARMOR:
                if (pickUpSFX)
                    pickUpSFX.Play(transform.position);
                Player.Instance.health.ArmorUp(amount);
                break;
            case PICKUPTYPE.SPECIAL:
                break;
            default:
                break;
        }
        Destroy(transform.parent.gameObject);
    }

    public override void OnExit(Collider other)
    {
    }
}
