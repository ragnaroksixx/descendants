﻿using UnityEngine;
using System.Collections;

public class HitBox : MonoBehaviour
{
    public int damage;
    public OnHitBehaviour onHit;
    public float continousTime;
    public float destroyTime;
    public GameObject destroyObj;
    public Collider col;
    public enum OnHitBehaviour
    {
        NONE,
        Destroy,
        Disable,
        Continuous,
        SetActive,
        DestoryIn
    }
    private void Awake()
    {
        if (onHit == OnHitBehaviour.DestoryIn)
        {
            Destroy(destroyObj, destroyTime);
        }
    }
    protected virtual void OnHit(Hurtbox hb)
    {
        if (!enabled) return;
        hb?.OnHit(this, damage);
        switch (onHit)
        {
            case OnHitBehaviour.NONE:
                break;
            case OnHitBehaviour.Destroy:
                Destroy(this.gameObject);
                break;
            case OnHitBehaviour.Disable:
                enabled = false;
                break;
            case OnHitBehaviour.Continuous:
                StartCoroutine(Flicker());
                break;
            case OnHitBehaviour.SetActive:
                gameObject.SetActive(false);
                break;
            default:
                break;
        }
    }
    protected virtual void OnCollisionEnter(Collision collision)
    {
        OnHit(collision.gameObject.GetComponent<Hurtbox>());
    }
    protected virtual void OnTriggerEnter(Collider other)
    {
        OnHit(other.gameObject.GetComponent<Hurtbox>());
    }
    IEnumerator Flicker()
    {
        col.enabled = false;
        yield return new WaitForSeconds(continousTime);
        col.enabled = true;
    }
}
