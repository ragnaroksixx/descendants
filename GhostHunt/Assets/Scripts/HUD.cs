﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System.Collections.Generic;

public class HUD : MonoBehaviour
{
    public Image grappleUI;
    Color normalColor;
    public Color badFlash;
    public Color useFlash;
    public Color replensihFlash;
    public CanvasGroup gunPickUpGroup;
    public TMP_Text textName, textDesc;
    public TMP_Text ammoText, hpText, levelText, armorText, killsText, scoresText;
    public PlayerHealth health;
    public Image weaponImage;
    public List<Image> positiveModifiers;
    public List<Image> negativeModifiers;
    private void Awake()
    {
        normalColor = grappleUI.color;
        ShowWeaponPickup(null);
        SetGun(null);
        UpdateModifiers();
    }
    private void Start()
    {
        levelText.text = "ZONE " + (LevelManager.levelIndex + 1);
        UpdateHealth();
    }
    public void CoolDownGrapple(float time)
    {
        grappleUI.fillAmount = 0;
        grappleUI.DOFillAmount(1, time)
            .OnComplete(() => { grappleUI.DOFlash(normalColor, replensihFlash); });
    }
    public void ClearGrappleUI()
    {
        grappleUI.fillAmount = 0;
    }
    public void GrappleNoHit()
    {
        grappleUI.DOKill();
        grappleUI.DOFlash(normalColor, badFlash, numLoops: 5);
    }
    public void ShowWeaponPickup(Weapon w)
    {
        if (w)
        {
            textName.text = w.data.displayName;
            textDesc.text = w.data.desrcText;
            gunPickUpGroup.DOKill();
            gunPickUpGroup.DOFade(1, 0.5f);
        }
        else
        {
            gunPickUpGroup.DOKill();
            gunPickUpGroup.DOFade(0, 0.25f);
        }
    }
    public void ShowModPickUp(ModifierPickUpVolume m)
    {
        if (m)
        {
            textName.text = m.data.displayName;
            textDesc.text = m.data.description;
            gunPickUpGroup.DOKill();
            gunPickUpGroup.DOFade(1, 0.5f);
        }
        else
        {
            gunPickUpGroup.DOKill();
            gunPickUpGroup.DOFade(0, 0.25f);
        }
    }
    public void SetGun(Weapon wd)
    {
        weaponImage.enabled = wd != null;
        if (wd)
            weaponImage.sprite = wd.data.weaponSprite;
        UpdateAmmo(wd == null ? -1 : wd.CurrentAmmo);
    }
    public void UpdateAmmo(int count)
    {
        if (count < 0)
        {
            weaponImage.enabled = false;
            ammoText.text = "";
            return;
        }
        weaponImage.enabled = true;
        ammoText.text = count.ToString();
    }
    public void UpdateHealth()
    {
        hpText.text = health.currentHP.ToString();
        armorText.text = health.armor.ToString();
    }
    public void Kill(int val)
    {
        killsText.text = "KILLS " + val;
    }
    public void UpdateModifiers()
    {
        for (int i = 0; i < ModifierHandler.Instance.maxModifiers; i++)
        {
            if (i < ModifierHandler.PositiveModifiers.Count)
            {
                positiveModifiers[i].sprite = ModifierHandler.PositiveModifiers[i].image;
                positiveModifiers[i].enabled = true;
            }
            else
                positiveModifiers[i].enabled = false;

            if (i < ModifierHandler.NegativeModifiers.Count)
            {
                negativeModifiers[i].sprite = ModifierHandler.NegativeModifiers[i].image;
                negativeModifiers[i].enabled = true;
            }
            else
                negativeModifiers[i].enabled = false;
        }
    }
    public void Score(int score)
    {
        scoresText.DOText(score.ToString("00000000"), 0.15f, true, ScrambleMode.Numerals);
    }
}
