﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Hurtbox : MonoBehaviour
{
    public HealthScript healthRef;
    public UnityAction onHitActions;
    public UnityEvent onHitEvents;
    public void OnHit(Object source, int amount)
    {
        if (!enabled) return;
        healthRef.TakeDamage(amount);
        onHitActions?.Invoke();
        onHitEvents?.Invoke();
    }
}
