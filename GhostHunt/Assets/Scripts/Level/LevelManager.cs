﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class LevelManager : MonoBehaviour
{
    public List<LevelData> levels;
    public static int levelIndex = 0;
    public LevelData currentLevel { get => levels[Mathf.Min(levelIndex, levels.Count - 1)]; }
    public LevelGenerator levelGen;
    public GameObject player;
    public static LevelManager Instance;
    public CanvasGroup gameOverScreen, levelStartScreen, levelStatsScreen;
    public TMP_Text levelText;
    public TMP_Text loadingText;
    public Image loadingBar;
    public static bool gameOver;
    public static bool zoneStarted = false;
    public ModifierHandler modiferHandler;
    public static int kills = 0;
    public static int score = 0;
    public WeaponObjectPool allWeapons;
    public ModifierObjectPool allMods;
    public static bool RestartingRound = false;
    private void Awake()
    {
        Instance = this;
        gameOver = false;
        RestartingRound = false;
        levelText.text = "ZONE " + (levelIndex + 1);
        if (ModifierHandler.Instance == null)
        {
            zoneStarted = false;
            modiferHandler.OnGameStart();
            WeaponHandler.equippedWeaponData = null;
            WeaponHandler.cacheAmmo = 0;
            Player.cacheARMOR = 0;
            Player.cacheHP = 0;
        }
    }
    public static void NextLevel()
    {
        zoneStarted = false;
        levelIndex++;
        SceneManager.LoadScene(1);
    }
    IEnumerator Start()
    {
        player.SetActive(false);
        AudioListener al = gameObject.AddComponent<AudioListener>();
        modiferHandler.UpdateAllVallues();
        yield return levelGen.SpawnMap(currentLevel);
        Player.Instance.weaponHandler.hud.Kill(kills);
        Player.Instance.weaponHandler.hud.Score(score);
        currentLevel.bgm.Play();
        levelStartScreen.DOFade(0, 1);
        Destroy(al);
        player.SetActive(true);
    }
    public static void GameOver()
    {
        gameOver = true;
        WeaponHandler.equippedWeaponData = null;
        WeaponHandler.cacheAmmo = 0;
        Instance.StartCoroutine(Instance.GameOverSequence());
    }
    public static void ReloadScene()
    {
        Time.timeScale = 1;
        RestartingRound = true;
        zoneStarted = false;
        ModifierHandler.Instance.OnGameReset();
        SceneManager.LoadScene(1);
    }
    public static void MainMenu()
    {
        Time.timeScale = 1;
        OnNewRun();
        AudioSystem.PlayBGM(null, Vector3.zero);
        SceneManager.LoadScene(0);
    }
    IEnumerator GameOverSequence()
    {
        Time.timeScale = 1;
        gameOverScreen.DOFade(1, 1);
        yield return new WaitForSeconds(2);
        OnNewRun();
        AudioSystem.PlayBGM(null, Vector3.zero);
        SceneManager.LoadScene(0);
        //NextLevel();
    }
    public static void OnNewRun()
    {
        levelIndex = 0;
        kills = 0;
        score = 0;
    }
    public static void Kill()
    {
        kills++;
        Instance.stats.kill++;
        Player.Instance.weaponHandler.hud.Kill(kills);
    }
    public static void Score(int val)
    {
        score += val;
        Player.Instance.weaponHandler.hud.Score(score);
    }
    public static void SetLoading(float percent, string text)
    {
        Instance.loadingText.text = text + "...";
        Instance.loadingBar.fillAmount = percent;
    }
    public LevelStats stats = new LevelStats();
    public StatsScreen screen;
    public static void EndLevel()
    {
        Instance.StartCoroutine(Instance.EndLevelCo());
    }
    IEnumerator EndLevelCo()
    {
        player.SetActive(false);
        screen.level.text = "Zone " + (levelIndex + 1).ToString() + " Results";
        levelStatsScreen.DOFade(1, 1);
        yield return new WaitForSeconds(1);
        stats.endTime = Time.time;
        AudioListener al = gameObject.AddComponent<AudioListener>();
        int seconds = Mathf.FloorToInt(stats.endTime - stats.startTime);
        int min = seconds / 60;
        seconds = seconds % 60;
        screen.time.Set("Time", min.ToString("00") + ":" + seconds.ToString("00"));
        screen.kills.Set("Kills", stats.kill.ToString());
        screen.modifiersGained.Set("Modifiers", stats.mods.ToString());
        screen.ammoUsed.Set("Shots Fired", stats.ammo.ToString());
        screen.damageDone.Set("Damage Dealt", stats.damage.ToString());
        yield return new WaitForSeconds(screen.time.Show());
        yield return new WaitForSeconds(screen.kills.Show());
        yield return new WaitForSeconds(screen.modifiersGained.Show());
        yield return new WaitForSeconds(screen.ammoUsed.Show());
        yield return new WaitForSeconds(screen.damageDone.Show());
        yield return new WaitForSeconds(2);
        NextLevel();
    }
}
public class LevelStats
{
    public float startTime, endTime;
    public int kill;
    public int mods;
    public int ammo;
    public int damage;
}
[System.Serializable]
public class StatsScreen
{
    public TMP_Text level;
    public StatsText time, kills, modifiersGained, ammoUsed, damageDone;
}
[System.Serializable]
public class StatsText
{
    public RectTransform root;
    public TMP_Text title, field;
    public void Set(string titleText, string fieldText)
    {
        title.text = titleText;
        field.text = fieldText;
    }
    public float Show()
    {
        root.DOScale(1, 0.3f).SetEase(Ease.InBounce);
        return 0.3f;
    }
}
