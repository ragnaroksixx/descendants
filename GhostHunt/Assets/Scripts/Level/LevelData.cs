﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu(fileName = "Level Data", menuName = "Level/LevelData")]
public class LevelData : ScriptableObject
{
    public LevelData baseData;
    public GameObject startingArea;
    [SerializeField]
    private GameObject[] possibleRooms;
    public SpawnData[] possibleEnemies;
    public int minRoomsToEnd;
    public GameObject deadEndHorizOverride, deadEndVertOverride;
    public CustomAudioClip bgm;
    public GameObject[] PossibleRooms
    {
        get
        {
            if (possibleRooms.Length == 0)
                return baseData.PossibleRooms;
            return possibleRooms;
        }
    }
    public GameObject GetDeadEnd(GameObject horzDefault, GameObject vertDeafult, bool vert)
    {
        if (!vert)
        {
            return deadEndHorizOverride == null ? horzDefault : deadEndHorizOverride;
        }
        else
        {
            return deadEndVertOverride == null ? vertDeafult : deadEndVertOverride;
        }
    }
}
