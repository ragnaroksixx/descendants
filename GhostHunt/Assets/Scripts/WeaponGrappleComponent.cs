﻿using UnityEngine;
using System.Collections;

public class WeaponGrappleComponent : GrappleObject
{
    public Weapon w;
    public override bool IgnoreGrapple()
    {
        return Player.Instance.weaponHandler.IsEquipped;
    }
    public override void OnGrappleAttach(GrappleHandler gh, RaycastHit hit)
    {
        base.OnGrappleAttach(gh, hit);
        Player.Instance.weaponHandler.EquipWeapon(w);
    }
}
