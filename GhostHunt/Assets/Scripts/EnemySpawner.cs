﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : ObjectSpawning
{
    public override IEnumerator Start()
    {
        spawnables = LevelManager.Instance.currentLevel.possibleEnemies;
        yield return base.Start();
    }
}
