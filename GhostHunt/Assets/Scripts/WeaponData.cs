﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Weapon Data", menuName = "Weapon/WeaponData")]
public class WeaponData : ScriptableObject
{
    public string displayName;
    public string desrcText;
    public ProjectileController projectilePrefab;
    public int shootsPerFire;
    public int ammoPerFire = 1;
    public float autofireRate;
    public float bulletSpread;
    public Weapon weaponPrefab;
    public int maxAmmo;
    public Sprite weaponSprite;
    public int throwDamage;
    public bool overrideColor;
    public Color color = Color.white;
    public ShootType type;
    public CustomAudioClip fireSFX;
    public enum ShootType
    {
        Manual,
        Automatic,
        Charge,
    }
}
