﻿using UnityEngine;
using System.Collections;

public class ZoneStartVolume : PickUpVolume
{
    public override void OnEnter(Collider other)
    {
        LevelManager.zoneStarted = true;
        LevelManager.Instance.stats.startTime = Time.time;
        Destroy(this.gameObject);
    }

    public override void OnExit(Collider other)
    {
    }
}
