﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Audio Clip")]
public class CustomAudioClip : ScriptableObject
{
    public AudioClip clip = null;
    public float volume = 1;
    public bool loop = false;
    public bool playWhilePaused = false;
    public AudioCategory category = AudioCategory.SFX;
    public float startTime = 0;

    public float Volume { get => volume; }
    public AudioClip Clip
    {
        get
        {
            return clip;
        }
    }
#if UNITY_EDITOR
    private void Awake()
    {
        if (clip == null)
        {
            if (Selection.activeObject is AudioClip)
            {
                name = Selection.activeObject.name;
                clip = Selection.activeObject as AudioClip;
            }
        }
    }
#endif
    public void Init()
    {
    }
    public void Play(float fadeInTime = -1)
    {
        Play(Vector3.zero, fadeInTime);
    }
    public void Play(Vector3 pos, float fadeInTime = -1)
    {
        if (category == AudioCategory.BGM)
        {
            AudioSystem.PlayBGM(this, pos, fadeInTime);
        }
        else
        {
            AudioSystem.PlaySFX(this, pos, fadeInTime);
        }
    }
}
