﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AudioObject : MonoBehaviour
{
    const float DEFAULT_AUDIO_FADE_DURATION = 1;
    AudioSource source;
    public CustomAudioClip clip;
    public Tweener fadeTween;
    public AudioSource Source
    {
        get
        {
            return source;
        }

        set
        {
            source = value;
        }
    }
    public bool isTransitioning;
    private void Update()
    {
        if (!source.isPlaying && !source.loop && !isTransitioning)
            AudioSystem.StopInstant(this);
    }
    public void FadeOut(float duration = -1)
    {
        duration = duration == -1 ? DEFAULT_AUDIO_FADE_DURATION : duration;

        if (fadeTween != null)
            fadeTween.Kill(false);

        isTransitioning = true;
        fadeTween = source.DOFade(0, duration).SetUpdate(source.ignoreListenerPause).OnComplete(
            () => { AudioSystem.StopInstant(this); isTransitioning = false; });

    }
    public void FadeIn(float duration = -1)
    {
        duration = duration == -1 ? DEFAULT_AUDIO_FADE_DURATION : duration;

        if (fadeTween != null)
            fadeTween.Kill(false);
        source.time = clip.startTime;
        source.Play();

        isTransitioning = true;
        fadeTween = source.DOFade(AudioSystem.GetVolume(clip), duration).OnComplete(
            () => { isTransitioning = false; });
    }

    public void Refresh(AudioSystem aSys)
    {
        if (fadeTween != null)
        {
            fadeTween.Kill(false);
        }
        source.volume = AudioSystem.GetVolume(clip);
    }
}
