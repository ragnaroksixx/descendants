﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioPool : Pool<AudioObject>
{
    public AudioPool(GameObject b, int num) : base(b, num)
    {
    }

    protected override void Init(AudioObject obj)
    {
        base.Init(obj);
        if (obj.Source == null)
            obj.Source = obj.GetComponent<AudioSource>();
        obj.fadeTween = null;
        obj.Source.Stop();
    }

    protected override void OnPop(AudioObject obj)
    {
        base.OnPop(obj);
        obj.fadeTween = null;
        obj.Source.Stop();
    }

    protected override void OnReturn(AudioObject obj)
    {
        base.OnReturn(obj);
        obj.fadeTween = null;
        obj.Source.Stop();
        obj.Source.ignoreListenerPause = false;
    }

    public void RefreshAll()
    {
        foreach (AudioObject item in activeItems)
        {
            item.Refresh(AudioSystem.Instance);
        }
    }
    public void StopAll()
    {
        List<AudioObject> activeTemp = new List<AudioObject>(activeItems.ToArray());
        foreach (AudioObject item in activeTemp)
        {
            Push(item);
        }
    }
}
